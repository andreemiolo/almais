<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
//for ($i=0;$i<5;$i++){
//    $pass = 'almais' . substr(uniqid(), -6);
//    print $pass;
//    print "<br>";
//    echo (bcrypt($pass));
//    print "<hr>";
//}
//die();
Auth::routes();

Route::get('/', 'HomeController@index')->name('cerebelo.index');
Route::get('/home', 'HomeController@index');

Route::group(['prefix' => '/', 'middleware' => ['auth']], function () {
    Route::get('/logout', function () {
        Auth::logout();
        return Redirect::route('cerebelo.index');
    })->name('cerebelo.logout');

    Route::get('/test', function () {
        print 'heeey';
    })->name('cerebelo.test');
});
