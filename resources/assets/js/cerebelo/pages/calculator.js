var constants = {};
var calculators = [];
var calculatorsData = [];
var calculatorsAccessories = {};
var $form;
var lastSave = 0;
var floatingBox = null;
var clickedCalculator = null;

function number_format(number, decimals, decPoint, thousandsSep) {
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number;
    var prec = !isFinite(+decimals) ? 2 : Math.abs(decimals);
    var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep;
    var dec = (typeof decPoint === 'undefined') ? '.' : decPoint;
    var s = '';

    var toFixedFix = function (n, prec) {
        var k = Math.pow(10, prec);
        return '' + (Math.round(n * k) / k)
                .toFixed(prec)
    };

    // @todo: for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }

    return s.join(dec);
}

const formatMoney = {
    cents: ',',
    thousands: '.'
};
Number.prototype.formatMoney = function (d, t, c) {

    var cents = '.', thousands = ',';
    if (formatMoney) {
        if (formatMoney.cents) {
            cents = formatMoney.cents;
        }
        if (formatMoney.thousands) {
            thousands = formatMoney.thousands;
        }
    }

    c = isNaN(c = Math.abs(c)) ? 2 : c;
    d = d == undefined ? cents : d;
    t = t == undefined ? thousands : t;

    var n = this,
        s = n < 0 ? "-" : "",
        i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

function time() {
    return Math.floor(((new Date).getTime() / 1000));
}

function forceCollapse(calculatorSelector) {
    $(calculatorSelector).find('.panel-body').toggle();
}

function number_format_locale(number, locale) {
    switch(locale) {
        case 'pt_BR': {
            return number_format(number, 2, ',', '.');
        }
    }

    return number_format(number, 2, '.', ',');
}

function applyMask(id) {
    var $inputs = $('#' + id).find('input.formatted');

    if ($inputs.length == 0) {
        setTimeout(function () {
            applyMask(id);
        }, 500);
        return;
    }

    $inputs.mask("#.##0,00", {reverse: true});
}

function cloneArrayOfObjects(arr) {
    var temp = [];

    for (var i = 0; i < arr.length; i++) {
        temp[i] = cloneObject(arr[i]);
    }

    return temp;
}

function cloneObject(obj) {
    if (obj === null || typeof obj !== 'object') {
        return obj;
    }

    var temp = obj.constructor(); // give temp the original obj's constructor
    for (var key in obj) {
        if (obj.hasOwnProperty(key)) {
            temp[key] = cloneObject(obj[key]);
        }
    }

    return temp;
}

function normalizeFloat(value) {
    value = value * 1.0;
    return (value.toFixed(2) * 1.0);
}

function initCalculator(mainUrl, kiloPrice) {
    constants.mainUrl = mainUrl;
    constants.kiloPrice = kiloPrice;
    constants.calculatorType = {
        aluminium: 0,
        glass: 1,
        acm: 2
    };
    constants.lines = [];

    Object.freeze(constants);
}

function startFloatingBox() {
    var text = '<div id="floating-box">' +
        '<p><span class="text-semibold">Gasto Total</span><span>R$ (( total | money ))</span></p>' +
        '<p><span class="text-semibold">Venda</span><span>R$ (( sellFinal | money ))</span></p>' +
        '<hr style="margin:5px 0 5px 0;" />' +
        // '<p id="a-receber"><span class="text-semibold">A Receber</span><span>R$ (( sell ))</span></p>' +
        '<p><span class="text-semibold">Lucro <small style="font-size:70%;">(( profit2 | money ))%</small></span><span>R$ (( profit | money ))</span></p>' +
        '</div>';
    $(document).ready(function () {
        new PNotify({
            text: text,
            hide: false,
            icon: false,
            addclass: "box-total",
            type: "success",
            nonblock: {
                nonblock: true
            }
        });
        drawFloatingBox();
    });
}

function formPrint() {
    formSave();
    $("#calculators").print();
}

function formSave() {
    lastSave = time();

    var $form = $("#form-calculators"), formData = $form.serializeArray();
    formData.push({name: "cost", value: floatingBox.sumSellFinalCalculatedValue()});
    $.ajax({
        type: "POST",
        url: $form.attr('action'),
        data: formData,
        success: function (response) {
            console.log(response);
        }
    });
}

function formAutosave() {
    $form = $("#form-calculators :input");
    $form.change(function () {
        console.log($(this).attr('name'));

        if (lastSave + 5 > time()) {
            return false;
        }

        formSave();
    });
}

function loadCalculators(data, accessories) {
    var calculatorData = null;
    if (data.hasOwnProperty('calculators')) {
        var calculators = data.calculators;

        calculatorsAccessories = accessories;

        for (var id in calculators) {
            if (calculators.hasOwnProperty(id)) {
                calculatorData = calculators[id];

                var line_id = calculatorData.lineDetails.line_id;

                // var calculator = addLine(id, price, kilo, meters, calculatorType, true, line_id);
                calculatorsData.push({
                    id: id,
                    data: calculatorData
                });
                console.log(calculatorData);
                insertNewCalculator(line_id, true, id);
            }
        }
    }
}

function setRecursively(object, item) {
    var getIndexById = function (items, id) {
        for (var i in items) {
            if (items.hasOwnProperty(i)) {
                console.log(items[i].id + '==' + id);
                if (items[i].id == id) {
                    return i;
                }
            }
        }

        return -1;
    };

    if (typeof item === 'object') {
        for (var subItem in item) {
            if (item.hasOwnProperty(subItem)) {

                var newSubItem = subItem;
                console.log(object, item, subItem, newSubItem);
                if (!isNaN(subItem) || typeof object[newSubItem] === 'undefined') {

                    var newSubItemTry = getIndexById(object, subItem);

                    console.log(newSubItemTry);
                    if (newSubItemTry >= 0) {
                        newSubItem = newSubItemTry;
                    }
                }

                if (typeof object[newSubItem] !== 'undefined') {
                    object[newSubItem] = setRecursively(object[newSubItem], item[subItem]);
                }
            }
        }
    } else {
        console.log(item);
        object = item;
    }

    return object;
}

function loadSavedDataListener() {
    if (calculatorsData) {
        for (var i in calculatorsData) {
            if (calculatorsData.hasOwnProperty(i)) {
                var id = calculatorsData[i].id;
                var calculator = getCalculator(id);

                if (!calculator || calculator.products.length <= 3) {
                    setTimeout(function () {
                        loadSavedDataListener();
                    }, 500);
                    return;
                }

                loadSavedData(id);
            }
        }
        //calculatorsData = [];
    }
}

loadSavedData = function (id) {
    var calculator = getCalculator(id);
    var calculatorData = getCalculatorData(id);

    if (calculatorsAccessories.hasOwnProperty(id)) {
        calculator.accessories = calculatorsAccessories[id].concat(calculator.accessories);
    }

    var property = null;
    for (var propertyId in calculatorData) {
        if (calculatorData.hasOwnProperty(propertyId)) {
            if (typeof calculatorData[propertyId] === 'object') {
                property = calculatorData[propertyId];

                setRecursively(calculator, property);
            }
        }
    }
};

function changeCalculatorType(calculatorTypeValue) {
    var $line_kilo = $('input[name=line_kilo]').parent().parent();

    $line_kilo.show();

    if (calculatorTypeValue == constants.calculatorType.aluminium) {
    } else if (calculatorTypeValue == constants.calculatorType.glass) {
        $line_kilo.hide();
    } else if (calculatorTypeValue == constants.calculatorType.acm) {
        $line_kilo.hide();
    }
}

function unshiftAccessory(calculator, accessory) {
    calculator.accessories.unshift(accessory);
    bindRemoveAccessories(calculator.id);
}

function retrieveAccessory(calculator, accessory_id) {
    calculator
        .$http
        .get(
            constants.mainUrl + '/calculator/data/2/' + calculator.lineDetails.calculatorType + '/' + accessory_id
        )
        .then(
            function (response) {
                var accessory = response.push();
                unshiftAccessory(calculator, accessory);
            }, function (response) {

            }
        );
}

function addAccessory() {
    var $select = $('select[name=accessory]');

    var selectedAccessory = $select.val();
    var selectData = $select.select2("data");

    var calculator = getCalculator(clickedCalculator);

    $select.select2("val", "");
    clickedCalculator = null;

    if (calculator.getAccessoryIndex(selectedAccessory) !== null) {
        swal({
            title: "O acessório já existe nesta calculadora",
            text: "Os acessórios só podem ser adicionados uma vez em cada calculadora. E o acessório " +
            "\"" + selectData.text + "\"" +
            "já está presente nesta.",
            confirmButtonColor: "#2196F3",
            type: "error"
        });

        return false;
    }

    retrieveAccessory(calculator, selectedAccessory);
}

function showModalAccessory(element) {
    clickedCalculator = $(element).parents('.panel.panel-white').eq(0).attr('id');

    $('#modal_acessorio').modal('show');
}

function getLineIndex(id) {
    for (var i in constants.lines) {
        if (constants.lines[i].id == id) {
            return i;
        }
    }

    return -1;
}

function getLine(id) {
    var index = getLineIndex(id);

    if (index >= 0) {
        return constants.lines[index];
    }

    return null;
}

function addLine(id, price, kilo, meters, calculator_type, local, line_id, name, installation, company_fixed_cost) {
    var newConstants = cloneObject(constants);

    newConstants.lines.push({
        id: id,
        line_id: line_id,
        name: name,
        price: price,
        installation: installation,
        kilo: kilo,
        meters: meters,
        calculatorType: calculator_type,
        companyFixedCost: company_fixed_cost
    });

    constants = newConstants;

    Object.freeze(constants);

    return addCalculator(id, local);
}

function getCalculatorIndex(id) {
    for (var i in calculators) {
        if (calculators[i].id == id) {
            return i;
        }
    }

    return -1;
}

function getCalculator(id) {
    var index = getCalculatorIndex(id);

    if (index >= 0) {
        return calculators[index].calculator;
    }

    return null;
}

function removeCalculator(id) {

    swal({
            title: "Excluir linha",
            text: "Tem certeza de que deseja excluir esta linha da calculadora atual?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Sim, excluir!",
            cancelButtonText: "Não, cancele por favor!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                swal({
                    title: "Excluído!",
                    text: "A linha selecionada foi excluída desta calculadora",
                    confirmButtonColor: "#66BB6A",
                    type: "success"
                });

                var cIndex = getCalculatorIndex(id);

                if (cIndex >= 0) {
                    calculators.splice(cIndex, 1);

                    var lIndex = getLineIndex(id);

                    var newConstants = cloneObject(constants);
                    newConstants.lines.splice(lIndex, 1);
                    constants = newConstants;
                    Object.freeze(constants);

                    $('#' + id).remove();

                    floatingBox.updateData();

                    removeCalculatorData(id);

                    return true;
                }

                return false;
            } else {
                swal({
                    title: "Cancelado!",
                    text: "Essa linha não foi excluída",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });

                return false;
            }
        });
}

function getCalculatorDataIndex(id) {
    for (var i in calculatorsData) {
        if (calculatorsData[i].id == id) {
            return i;
        }
    }

    return -1;
}

function getCalculatorData(id) {
    var index = getCalculatorDataIndex(id);

    if (index >= 0) {
        return calculatorsData[index];
    }

    return null;
}

function removeCalculatorData(id) {
    var index = getCalculatorDataIndex(id);
/*
TODO: fix that
 */
return;
    if (index >= 0) {
        calculatorsData.splice(index, 1);

        return true;
    }

    return false;
}

function bindRemoveAccessories(id) {
    $("#" + id + " a.btnMinus").unbind('click').bind('click', function (event) {
        event.preventDefault();

        var $tr = $(this).parent().parent();

        var accessoryId = $tr.find("input[type=hidden]").val();
        var calculator = getCalculator(id);
        var accessoryIndex = calculator.getAccessoryIndex(accessoryId);

        calculator.accessories.splice(accessoryIndex, 1);
    });

    formAutosave();
}

function newLine(clickedElement) {
    var $that = $(clickedElement);
    var $parent = $that.parent().parent();

    var line_id = $parent.find('[name=lines]').val();
    var calculator_type = $parent.find('[name=calculator_type]:checked').val();
    var meters = $parent.find('[name=line_meters]').val();
    var kilo = $parent.find('[name=line_kilo]').val();

    var $select = $('select[name=lines]');
    $select.select2("val", "");

    $parent.find('input[name=line_meters]').val('');
    $parent.find('input[name=line_kilo]').val('');

    return insertNewCalculator(line_id);
}

function insertNewCalculator(line_id, local, id) {
    $.post(
        constants.mainUrl + '/calculator/new',
        {
            id: id,
            line_id: line_id
        },
        function (data) {
            if (data.error) {
                swal({
                    title: "Erro ao inserir linha",
                    text: "Algum erro ocorreu ao tentar inserir esta linha na calculadora. Entre em contato com o suporte.",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
                return false;
            }

            $('#calculators').append(data.contents);
            // addLine(data.id, data.price, data.kilo, data.meters, calculatorType, local, line_id, data.name, data.installation);
            addLine(data.id, data.price, 0, 0, data.calculatorType, local, line_id, data.name, data.installation, data.company_fixed_cost);
            applyMask(data.id);
        }
    );
}

$(function () {
    $('#addLine').bind('click', function (e) {
        e.preventDefault();
        newLine(this);
    });
});

$(function () {

    /* ------------------------------------------------------------------------------
     *
     *  # Form Layouts
     *
     * ---------------------------------------------------------------------------- */

    // Basic
    $('.select').select2();


    //
    // Select with icons
    //

    // Initialize
    $(".select-icons").select2({
        formatResult: iconFormat,
        minimumResultsForSearch: "-1",
        width: '100%',
        formatSelection: iconFormat,
        escapeMarkup: function (m) {
            return m;
        }
    });

    // Format icons
    function iconFormat(state) {
        var originalOption = state.element;
        return "<i class='icon-" + $(originalOption).data('icon') + "'></i>" + state.text;
    }

    // Styled form components
    // ------------------------------

    // Checkboxes, radios
    $(".styled").uniform({radioClass: 'choice'});

    // File input
    $(".file-styled").uniform({
        fileButtonHtml: '<i class="icon-googleplus5"></i>',
        wrapperClass: 'bg-warning'
    });


    /* ------------------------------------------------------------------------------
     *
     *  # Modais
     *
     * ---------------------------------------------------------------------------- */

    // ALerts e Modais de Confirmação
    // ------------------------------

    // Salvar
    $('.salvar').on('click', function (event) {
        event.preventDefault();

        swal(
            {
                title: "Tem certeza que deseja salvar?",
                text: "As alterações não poderão ser desfeitas!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#EF5350",
                confirmButtonText: "Sim, desejo salvar!",
                cancelButtonText: "Não, cancele por favor!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function (isConfirm) {
                if (isConfirm) {
                    swal({
                        title: "Salvo!",
                        text: "Registro salvo com sucesso!",
                        confirmButtonColor: "#66BB6A",
                        type: "success"
                    });
                    formAutosave();
                    formSave();
                }
                else {
                    swal({
                        title: "Cancelado",
                        text: "Esse registro não foi salvo.",
                        confirmButtonColor: "#2196F3",
                        type: "error"
                    });
                }
            }
        );
    });
});
