// Vue.component('services-item', {
//     props: ['service'],
//     template: '<li v-on:click="toggleActive(service)" v-bind:class="{\'active\': service.active}">' +
//     '{{service.name}} <span>{{service.price | currency}}</span>' +
//     '</li>'
// });

function addCalculator(id, local) {

    Vue.filter('currency', {
        read: function (value) {
            return '$' + value.toFixed(2)
        },
        write: function (value) {
            var number = +value.replace(/[^\d.]/g, '')
            return isNaN(number) ? 0 : number
        }
    });

    var lineDetails = getLine(id);
    var calculator = new Vue({
        el: '#' + id,
        delimiters: ["((", "))"],
        filters: {
            money: function (e, cents, thousands) {
                return e.formatMoney(cents, thousands);
            }
        },
        methods: {
            getQuantity: function (object) {
                var quantity = 0;

                if (object.hasOwnProperty('quantity')) {
                    quantity = object.quantity;
                } else {
                    if (object.type == 0 && this.lineDetails.calculatorType == constants.calculatorType.aluminium) {
                        quantity = this.lineDetails.kilo;
                    } else {
                        quantity = this.lineDetails.meters;
                    }
                }

                return quantity;
            },
            hasMarkup: function () {
                // return !(this.markup === "" || this.markup < 0);
                return true;
            },
            calc: function (object, toMoney) {
                var quantity = this.getQuantity(object);

                floatingBox.updateData();

                var calc = ((object.price * quantity).toFixed(2) * 1.0);
                if (toMoney) {
                    calc = number_format_locale(calc, 'pt_BR');
                }

                return calc;
            },
            call: function (f, toMoney) {
                var a = null;
                eval("a = this." + f + "();");
                a = a * 1.0;

                a = (a.toFixed(2) * 1.0);
                if (toMoney) {
                    a = number_format_locale(a, 'pt_BR');
                }

                return a;
            },
            getAccessoryIndex: function (id) {
                for (var i in this.accessories) {
                    if (this.accessories.hasOwnProperty(i)) {
                        if (this.accessories[i].id == id) {
                            return i;
                        }
                    }
                }

                return null;
            },
            updateKiloPriceMarkup: function () {
                var aluminiumQuantity = this.getQuantity(this.products[0]);
                /*var sellPrice = this.calcSellTotal() - (this.kiloPrice * aluminiumQuantity);

                 this.markup = (this.markup * 1.0).toFixed(2) * 1.0;

                 var finalValue;
                 // finalValue = (1.0 + (this.markup / 100.0)) * this.calcTotalTotal();
                 finalValue = this.calcTotalTotal() / (1 - ((this.markup / 100.0).toFixed(4) * 1.0));
                 finalValue = finalValue.toFixed(2) * 1.0;

                 var aluminiumTotal = finalValue - sellPrice;

                 var total = (aluminiumTotal / aluminiumQuantity).toFixed(2) * 1.0;*/

                var total = normalizeFloat(this.calcSellFinalTotal() / aluminiumQuantity);

                this.kiloPrice = total;
                return total;
            },

            updateFormatted: function (field, id, event) {
                var value = event.target.value;

                var findPosition = function (objects, id) {
                    for (var i in objects) {
                        if (objects.hasOwnProperty(i)) {
                            if (objects[i].id == id) {
                                return i;
                            }
                        }
                    }

                    return -1;
                };

                var position = findPosition(this[field], id);

                console.log('set', field, position, value);
                // filter here
                if (position >= 0) {
                    // this[field][position].price = format(value);
                    this[field][position].price_formatted = value;
                }
            },

            calcTotalPrice: function (items) {
                var total = 0.0;
                for (var i in items) {
                    if (items.hasOwnProperty(i)) {
                        total += items[i].price;
                    }
                }
                return total;
            },
            calcTotalQuantity: function (items) {
                var total = 0.0;
                for (var i in items) {
                    if (items.hasOwnProperty(i)) {
                        total += this.getQuantity(items[i]);
                    }
                }
                return total;
            },

            calcTotal: function (type) {
                var total = 0.0;
                for (var i = 0; i < type.length; i++) {
                    total += (1.0 * type[i].price) * (1.0 * this.getQuantity(type[i]));
                }
                return total;
            },

            calcTotalAccessories: function (toMoney) {
                var response = this.calcTotal(this.accessories);
                if (toMoney) {
                    response = number_format_locale(response, 'pt_BR');
                }

                return response;
            },
            calcTotalDeliveries: function (toMoney) {
                var response = this.calcTotal(this.deliveries);
                if (toMoney) {
                    response = number_format_locale(response, 'pt_BR');
                }

                return response;
            },
            calcTotalInstallation: function () {
                return normalizeFloat(this.lineDetails.meters * this.calculated.cost_installation.value);
            },
            calcTotalInstallation2: function () {
                return normalizeFloat(this.lineDetails.meters * this.calculated.cost_installation2.value);
            },
            calcTotalInstallations: function () {
                return normalizeFloat(this.calcTotalInstallation() + this.calcTotalInstallation2());
            },

            calcTotalTotal: function (toMoney) {
                var response = (
                (
                    this.calcTotal(this.products) +
                    this.calcTotalDeliveries() +
                    this.calcTotalAccessories() +
                    this.calcTotalInstallations() +
                    // this.calcCommissionTotal() +
                    // this.calcTaxesTotal() +
                    0
                ).toFixed(2) * 1.0);
                if (toMoney) {
                    response = number_format_locale(response, 'pt_BR');
                }

                return response;
            },

            calcVariableTotal: function () {
                return this.calcTotalTotal() - this.calc(this.products[this.products.length - 2]);
            },
            calcSellTotal: function () {
                var total = 0.0;
                // if (!this.hasMarkup()) {
                //     total = this.getQuantity(this.products[0]) * this.calculated.kiloPrice.value;
                // } else {
                this.markup = (this.markup * 1.0).toFixed(2) * 1.0;
                total = this.calcTotalTotal() / (1.0 - ((this.markup / 100.0).toFixed(4) * 1.0));
                total = total.toFixed(2) * 1.0;
                // }
                return total;
            },
            calcSellFinalTotal: function () {
                var extra = this.calcCommissionAndTaxesTotal();

                return normalizeFloat(this.calcSellTotal() + (extra.commissionValue + extra.taxeValue));
            },
            calcMarginPercent: function () {
                return normalizeFloat((this.calcTotalTotal() / this.lineDetails.companyFixedCost) * 100.0);
            },
            calcCommissionAndTaxesTotal: function () {
                var commissionPercent = (this.calculated.commission.percent / 100.0).toFixed(4) * 1.0;
                var taxePercent = (
                        (this.calculated.taxes.rate / 100.0) *
                        (this.calculated.taxes.percent / 100.0)
                    ).toFixed(4) * 1.0;
                // console.log("Comissão " + commissionPercent);
                // console.log("Imposto " + taxePercent);

                var totalPercent = commissionPercent + taxePercent;
                // console.log("Porcentagem total " + totalPercent);

                var finalValue = (this.calcSellTotal() / (1.0 - totalPercent)).toFixed(2) * 1.0;
                // console.log("Valor final " + finalValue);
                var increment = (finalValue - this.calcSellTotal()).toFixed(2) * 1.0;
                // console.log("Incremento " + increment);

                var taxeIncrementPercent = (taxePercent / totalPercent).toFixed(4) * 1.0;
                var commissionIncrementPercent = 1.0 - taxeIncrementPercent;
                // console.log("Comissão relativa " + commissionIncrementPercent);
                // console.log("Imposto relativa " + taxeIncrementPercent);

                var taxeValue = (taxeIncrementPercent * increment).toFixed(2) * 1.0;
                var commissionValue = (increment - taxeValue).toFixed(2) * 1.0;
                // console.log("Comissão real " + taxeValue);
                // console.log("Imposto real " + commissionValue);

                return {
                    taxeValue: taxeValue,
                    commissionValue: commissionValue
                };
            },
            calcCommissionTotal: function () {
                return this.calcCommissionAndTaxesTotal().commissionValue;
            },
            calcTaxesTotal: function () {
                return this.calcCommissionAndTaxesTotal().taxeValue;
                return 0;
            },
            calcProfitTotal: function () {
                return this.calcSellTotal() - this.calcTotalTotal();
            },
            calcProfitPercent: function () {
                /*var value = (
                 (
                 (1 - (this.calcTotalTotal() / this.calcSellTotal())) * 100
                 ).toFixed(3) * 1.0);*/
                // var value = (((100.0 * this.calcSellTotal()) / this.calcTotalTotal()) - 100.0).toFixed(3) * 1.0;
                var value = (100.0 - ((100.0 * this.calcTotalTotal()) / this.calcSellTotal())).toFixed(3) * 1.0;

                if (isNaN(value) && !isFinite(value)) {
                    return 0;
                }
                return value;
            }
        },
        data: {
            id: id,
            lineDetails: lineDetails,
            linePrice: lineDetails.price,
            kiloPriceDefault: constants.kiloPrice,
            kiloPrice: constants.kiloPrice,
            kiloPriceMarkup: constants.kiloPrice,
            markup: 0,
            calculated: {},
            products: [
                // {
                //     name: "Alumínio",
                //     price: 13.00,
                //     calculated: false,
                //     type: 0
                // },
                // {
                //     name: "Frete",
                //     price: 0.50,
                //     calculated: false,
                //     type: 0
                // },
                // {
                //     name: "Anodização / Pintura",
                //     price: 2.50,
                //     calculated: false,
                //     type: 0
                // },
                // {
                //     name: "Custo de instalação",
                //     price: 20.00,
                //     calculated: false,
                //     type: 1
                // },
                // {
                //     name: "Custo de instalação 2",
                //     price: 0.00,
                //     calculated: false,
                //     type: 1
                // }
            ],
            accessories: [
                // {
                //     id: 1,
                //     name: "Silicone",
                //     quantity: 0,
                //     taxe: true,
                //     print: true,
                //     price: 0.00
                // },
                // {
                //     id: 2,
                //     name: "Sikadur",
                //     quantity: 0,
                //     taxe: true,
                //     print: true,
                //     price: 0.00
                // },
                // {
                //     id: 3,
                //     name: "Chumbador",
                //     quantity: 0,
                //     taxe: true,
                //     print: true,
                //     price: 0.00
                // },
                // {
                //     id: 4,
                //     name: "Parafuso",
                //     quantity: 0,
                //     taxe: true,
                //     print: true,
                //     price: 0.00
                // },
                // {
                //     id: 5,
                //     name: "Fita",
                //     quantity: 0,
                //     taxe: true,
                //     print: true,
                //     price: 0.00
                // },
                // {
                //     id: 6,
                //     name: "Guarnição",
                //     quantity: 0,
                //     taxe: true,
                //     print: true,
                //     price: 0.00
                // },
                // {
                //     id: 7,
                //     name: "Vulcanização",
                //     quantity: 0,
                //     taxe: true,
                //     print: true,
                //     price: 0.00
                // },
                // {
                //     id: 8,
                //     name: "Curvatura",
                //     quantity: 0,
                //     taxe: true,
                //     print: true,
                //     price: 0.00
                // },
                // {
                //     id: 9,
                //     name: "Chapas",
                //     quantity: 0,
                //     taxe: true,
                //     print: true,
                //     price: 0.00
                // },
                // {
                //     id: 10,
                //     name: "Grelhas",
                //     quantity: 0,
                //     taxe: true,
                //     print: true,
                //     price: 0.00
                // },
                // {
                //     id: 11,
                //     name: "Routiamento de ACM",
                //     quantity: 0,
                //     taxe: true,
                //     print: true,
                //     price: 0.00
                // }
            ],
            deliveries: [
                {
                    id: 1,
                    name: "Des. Viagem Instalação",
                    quantity: 0,
                    price: 0.00
                },
                {
                    id: 2,
                    name: "Des. Viagem Gerência",
                    quantity: 0,
                    price: 0.00
                },
                {
                    id: 3,
                    name: "Des. Viagem Diretoria",
                    quantity: 0,
                    price: 0.00
                },
                {
                    id: 4,
                    name: "Reg. de Empreiteiros",
                    quantity: 0,
                    price: 0.00
                },
                {
                    id: 5,
                    name: "Outros",
                    quantity: 0,
                    price: 0.00
                }
            ]
        },
        created: function () {
            this.products.push({
                id: "fixedCost",
                name: "Custo fixo",
                price: this.linePrice,
                price_formatted: number_format(this.linePrice, 2, ',', '.'),
                edited: false,
                calculated: false,
                type: 0
            });
            this.products.push({
                id: "accessoriesCost",
                name: "Custo dos acessórios",
                edited: false,
                calculated: true,
                calculatedValue: 'calcTotalAccessories',
                type: 0
            });

            var that = this;

            var specificProducts = [], data = getCalculatorData(id);
            if (data) {
                var savedProducts = data.data.products;
                for (var i in savedProducts) {
                    if (savedProducts.hasOwnProperty(i)) {
                        if (!isNaN(i)) {
                            specificProducts.push(i);
                        }
                    }
                }
            }
            console.log(specificProducts, data);
            this.$http.get(
                constants.mainUrl +
                '/calculator/data/1/' +
                lineDetails.calculatorType +
                (
                    specificProducts.length ?
                        '/' + specificProducts.join()
                        : ''
                )
            ).then(
                function (response) {
                    that.products = response.data.concat(that.products);
                    formAutosave();
                }, function (response) {

                }
            );

            this.accessories.push({
                id: "others1",
                name: "Outros 1",
                quantity: 0,
                taxe: true,
                print: false,
                price: 0.00
            });
            this.accessories.push({
                id: "others2",
                name: "Outros 2",
                quantity: 0,
                taxe: true,
                print: false,
                price: 0.00
            });

            if (!local) {
                this.$http.get(constants.mainUrl + '/calculator/data/2/' + lineDetails.calculatorType).then(
                    function (response) {
                        for (var i in response.data) {
                            if (response.data.hasOwnProperty(i)) {
                                unshiftAccessory(this, response.data[i]);
                            }
                        }
                    }, function (response) {

                    }
                );
            } else {
                /*$(function() {
                 loadSavedData(id);
                 });*/
                console.log(id);
            }

            this.calculated = {

                // Se for terceirizar, ambos devem ser passados para produtos para poderem entrar no valor variável
                cost_installation: {
                    name: "Custo de instalação",
                    value: lineDetails.installation,
                    calculatedValue: 'calcTotalInstallation'
                },
                cost_installation2: {
                    name: "Custo de instalação 2",
                    value: 0,
                    calculatedValue: 'calcTotalInstallation2'
                },

                delivery: {
                    name: "Frete Hospedagem Passagem",
                    calculatedValue: "calcTotalDeliveries"
                },
                commission: {
                    name: "Comissão",
                    percent: 0.0,
                    calculatedValue: "calcCommissionTotal"
                },
                taxes: {
                    name: "Impostos",
                    rate: 100.0,
                    percent: 11.0,
                    calculatedValue: "calcTaxesTotal"
                },
                variable: {
                    name: "Custo Variável Total",
                    calculatedValue: "calcVariableTotal"
                },
                total: {
                    name: "Custo Total",
                    calculatedValue: "calcTotalTotal"
                },
                sell: {
                    name: "Preço de Venda (recebido)",
                    calculatedValue: "calcSellTotal"
                },
                sellFinal: {
                    name: "Preço de Venda (final)",
                    calculatedValue: "calcSellFinalTotal"
                },
                taxes_margin: {
                    name: "Margem de contribuição (%)",
                    calculatedValue: "calcMarginPercent"
                },
                kiloPrice: {
                    name: "Preço " + (lineDetails.calculatorType == 0 ? 'kilo' : 'metro'),
                    // value: this.kiloPrice
                    calculatedValue: 'updateKiloPriceMarkup'
                },
                profit: {
                    name: "Lucro",
                    calculatedValue: "calcProfitTotal",
                    calculatedValue2: "calcProfitPercent"
                }
            };
        },
        watch: {
            kiloPrice: {
                handler: function () {
                    this.calculated.kiloPrice.value = this.kiloPrice;
                    if (!this.hasMarkup()) {
                        this.kiloPriceDefault = this.kiloPrice;
                    }
                },
                deep: true
            },
            markup: {
                handler: function () {
                    this.updateKiloPriceMarkup();
                },
                deep: true
            },
            linePrice: {
                handler: function () {
                    this.products[this.products.length - 2].price = this.linePrice;
                },
                deep: true
            },
            products: {
                handler: function () {
                    var format = function (value) {
                        var formatted = value;

                        if (typeof value === 'string') {
                            formatted = parseFloat(value.replace(',', '').replace('.', '')) / 100.0;
                        }

                        if (isNaN(formatted)) {
                            return 0;
                        }

                        return formatted;
                    };

                    for (var i in this.products) {
                        if (this.products.hasOwnProperty(i)) {
                            this.products[i].price = format(this.products[i].price_formatted);
                        }
                    }
                },
                deep: true
            }
        }
    });

    calculators.push({
        id: id,
        calculator: calculator
    });

    floatingBox.updateData();

    return calculator;
}

function drawFloatingBox() {
    floatingBox = new Vue({
        el: '#floating-box',
        delimiters: ["((", "))"],
        filters: {
            money: function (e, cents, thousands) {
                return e.formatMoney(cents, thousands);
            }
        },
        data: {
            total: 0.0,
            sell: 0.0,
            sellFinal: 0.0,
            profit: 0.0,
            profit2: 0.0
        },
        methods: {
            updateData: function () {
                this.total = normalizeFloat(this.sumTotalCalculatedValue());
                this.sell = normalizeFloat(this.sumSellCalculatedValue());
                this.sellFinal = normalizeFloat(this.sumSellFinalCalculatedValue());
                this.profit = normalizeFloat(this.sumProfitCalculatedValue());
                this.profit2 = normalizeFloat(this.sumProfitCalculatedValue2());
            },
            sumTotalCalculatedValue: function () {
                var total = 0.0, calc;
                for (var i in calculators) {
                    if (calculators.hasOwnProperty(i)) {
                        calc = calculators[i].calculator;

                        total += calc.call(calc.calculated.total.calculatedValue);
                    }
                }

                return total;
            },
            sumSellCalculatedValue: function () {
                var total = 0.0, calc;
                for (var i in calculators) {
                    if (calculators.hasOwnProperty(i)) {
                        calc = calculators[i].calculator;

                        total += calc.call(calc.calculated.sell.calculatedValue);
                    }
                }

                return total;
            },
            sumSellFinalCalculatedValue: function () {
                var total = 0.0, calc;
                for (var i in calculators) {
                    if (calculators.hasOwnProperty(i)) {
                        calc = calculators[i].calculator;

                        total += calc.call(calc.calculated.sellFinal.calculatedValue);
                    }
                }

                return total;
            },
            sumProfitCalculatedValue: function () {
                var total = 0.0, calc;
                for (var i in calculators) {
                    if (calculators.hasOwnProperty(i)) {
                        calc = calculators[i].calculator;

                        total += calc.call(calc.calculated.profit.calculatedValue);
                    }
                }

                return total;
            },
            sumProfitCalculatedValue2: function () {
                var total = 0.0, calc, count = 0;
                for (var i in calculators) {
                    if (calculators.hasOwnProperty(i)) {
                        calc = calculators[i].calculator;
                        count++;

                        total += calc.call(calc.calculated.profit.calculatedValue2);
                    }
                }

                if (count == 0) {
                    return 0;
                }

                return total / count;
            }
        }
    });
}