Vue.component('services-item', {
    props: ['service'],
    template: '<li v-on:click="toggleActive(service)" v-bind:class="{\'active\': service.active}">' +
    '{{service.name}} <span>{{service.price | currency}}</span>' +
    '</li>'
});

window.onload = function () {
    function cloneArrayOfObjects(arr) {
        var temp = [];

        for (var i = 0; i < arr.length; i++) {
            temp[i] = cloneObject(arr[i]);
        }

        return temp;
    }

    function cloneObject(obj) {
        if (obj === null || typeof obj !== 'object') {
            return obj;
        }

        var temp = obj.constructor(); // give temp the original obj's constructor
        for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
                temp[key] = cloneObject(obj[key]);
            }
        }

        return temp;
    }

    var calculator = new Vue({
        el: '#calculator',
        methods: {
            calc: function (object) {
                return ((object.price * object.quantity).toFixed(2) * 1.0);
            },
            calcTotalPrice: function (items) {
                var total = 0.0;
                for (var i in items) {
                    if (items.hasOwnProperty(i)) {
                        total += items[i].price;
                    }
                }
                return total;
            },
            calcTotalQuantity: function (items) {
                var total = 0.0;
                for (var i in items) {
                    if (items.hasOwnProperty(i)) {
                        total += items[i].quantity;
                    }
                }
                return total;
            },
            calcTotal: function (type) {
                var total = 0.0;
                for (var i = 0; i < type.length; i++) {
                    total += (1.0 * type[i].price) * (1.0 * type[i].quantity);
                }
                return total;
            },
            calcTotalAccessories: function () {
                return this.calcTotal(this.accessories);
            },
            calcTotalDeliveries: function () {
                return this.calcTotal(this.deliveries);
            },
            calcTotalTotal: function () {
                return ((this.calcTotal(this.products) +
                    this.calcTotalDeliveries() +
                    this.calcCommissionTotal() +
                    this.calcTotalAccessories() +
                    this.calcTaxesTotal()).toFixed(2) * 1.0);
            },
            calcVariableTotal: function() {
                return this.calcTotalTotal() - this.calc(this.products[this.products.length - 2]);
            },
            calcSellTotal: function() {
                return this.products[0].quantity * this.calculated.kiloPrice.value;
            },
            calcMarginTotal: function() {
                return this.calcSellTotal() - this.calcVariableTotal();
            },
            calcMarginPercent: function() {
                return this.calcMarginTotal() / this.calcSellTotal();
            },
            calcCommissionTotal: function() {
                //todo : ovo e galinha
                // return this.calcTotalTotal() * (this.calculated.commission.percent / 100);
                return this.calcSellTotal() * (this.calculated.commission.percent / 100);
            },
            calcTaxesTotal: function() {
                return ((this.calcSellTotal() * (this.calculated.taxes.rate / 100)) * (this.calculated.taxes.percent / 100));
            },
            calcProfitTotal: function() {
                return this.calcSellTotal() - this.calcTotalTotal();
            },
            calcProfitPercent: function() {
                var value = (
                    (
                        (1 - (this.calcTotalTotal() / this.calcSellTotal())) * 100
                    ).toFixed(3) * 1.0);
                if (isNaN(value) && !isFinite(value)) {
                    return 0;
                }
                return value;
            },
            call: function (f) {
                var a = null;
                eval("a = this." + f + "();");
                a = a * 1.0;
                return (a.toFixed(2) * 1.0);
            }
        },
        data: {
            default: {},
            linePrice: "30.6",
            kiloPrice: "74.19",
            products: [
                {
                    name: "Alumínio",
                    quantity: 0,
                    price: 13.00,
                    edited: false,
                    calculated: false
                },
                {
                    name: "Frete",
                    quantity: 0,
                    price: 0.50,
                    edited: false,
                    calculated: false
                },
                {
                    name: "Anodização / Pintura",
                    quantity: 0,
                    price: 2.50,
                    edited: false,
                    calculated: false
                },
                {
                    name: "Custo de instalação",
                    quantity: 0,
                    price: 20.00,
                    edited: false,
                    calculated: false
                },
                {
                    name: "Custo de instalação 2",
                    quantity: 0,
                    price: 0.00,
                    edited: false,
                    calculated: false
                }
            ],
            accessories: [
                {
                    name: "Silicone",
                    quantity: 0,
                    price: 0.00
                },
                {
                    name: "Sikadur",
                    quantity: 0,
                    price: 0.00
                },
                {
                    name: "Chumbador",
                    quantity: 0,
                    price: 0.00
                },
                {
                    name: "Parafuso",
                    quantity: 0,
                    price: 0.00
                },
                {
                    name: "Fita",
                    quantity: 0,
                    price: 0.00
                },
                {
                    name: "Guarnição",
                    quantity: 0,
                    price: 0.00
                },
                {
                    name: "Vulcanização",
                    quantity: 0,
                    price: 0.00
                },
                {
                    name: "Curvatura",
                    quantity: 0,
                    price: 0.00
                },
                {
                    name: "Chapas",
                    quantity: 0,
                    price: 0.00
                },
                {
                    name: "Grelhas",
                    quantity: 0,
                    price: 0.00
                },
                {
                    name: "Routiamento de ACM",
                    quantity: 0,
                    price: 0.00
                },
                {
                    name: "Outros 1",
                    quantity: 0,
                    price: 0.00
                },
                {
                    name: "Outros 2",
                    quantity: 0,
                    price: 0.00
                }
            ],
            calculated: {},
            deliveries: [
                {
                    name: "Des. Viagem Instalação",
                    quantity: 0,
                    price: 0.00
                },
                {
                    name: "Des. Viagem Gerência",
                    quantity: 0,
                    price: 0.00
                },
                {
                    name: "Des. Viagem Diretoria",
                    quantity: 0,
                    price: 0.00
                },
                {
                    name: "Reg. de Empreiteiros",
                    quantity: 0,
                    price: 0.00
                },
                {
                    name: "Outros",
                    quantity: 0,
                    price: 0.00
                }
            ]
        },
        created: function () {
            this.products.push({
                name: "Custo fixo",
                quantity: 0,
                price: this.linePrice,
                edited: false,
                calculated: false
            });
            this.products.push({
                name: "Custo dos acessórios",
                quantity: 0,
                price: 0.00,
                edited: false,
                calculated: true,
                calculatedValue: 'calcTotalAccessories'
            });

            this.default.products = cloneArrayOfObjects(this.products.slice());
            this.calculated = {
                delivery: {
                    name: "Frete Hospedagem Passagem",
                    calculatedValue: "calcTotalDeliveries"
                },
                commission: {
                    name: "Comissão",
                    percent: 0.0,
                    calculatedValue: "calcCommissionTotal"
                },
                taxes: {
                    name: "Impostos",
                    rate: 100.0,
                    percent: 11.0,
                    calculatedValue: "calcTaxesTotal"
                },
                variable: {
                    name: "Custo Variável Total",
                    calculatedValue: "calcVariableTotal"
                },
                total: {
                    name: "Custo Total",
                    calculatedValue: "calcTotalTotal"
                },
                sell: {
                    name: "Preço de Venda",
                    calculatedValue: "calcSellTotal"
                },
                taxes_margin: {
                    name: "Margem de contribuição",
                    calculatedValue: "calcMarginTotal",
                    calculatedValue2: "calcMarginPercent"
                },
                kiloPrice: {
                    name: "Preço Kilo",
                    value: this.kiloPrice
                },
                profit: {
                    name: "Lucro",
                    calculatedValue: "calcProfitTotal",
                    calculatedValue2: "calcProfitPercent"
                }
            }
        },
        watch: {
            products: {
                handler: function () {
                    var i = 0, lastQuantity = null;

                    for (i = 0; i < this.products.length; i++) {

                            if (this.products[i].quantity != this.default.products[i].quantity) {

                                this.products[i].edited = true;
                                lastQuantity = (this.products[i].quantity * 1.0);
                                this.default.products[i].quantity = lastQuantity;
                                break;
                            }
                    }

                    if (lastQuantity !== null) {
                        for (var j = i; j < this.products.length; j++) {
                            if (!this.products[j].edited) {
                                this.default.products[j].quantity = lastQuantity;
                                this.products[j].quantity = lastQuantity;
                            }
                        }
                    }
                },
                deep: true
            },
            kiloPrice: {
                handler: function () {
                    this.calculated.kiloPrice.value = this.kiloPrice;
                },
                deep: true
            },
            linePrice: {
                handler: function () {
                    this.products[this.products.length - 2].price = this.linePrice;
                },
                deep: true
            }
        }
    });
};