CREATE SCHEMA almais;
USE almais;

CREATE TABLE `lines` (
  id         INT(11) UNSIGNED                               NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name       VARCHAR(255)                                   NOT NULL,
  price_buy  DECIMAL(10, 2) DEFAULT 0.00                    NOT NULL,
  price_sell DECIMAL(10, 2) DEFAULT 0.00                    NOT NULL,
  active     TINYINT(1) DEFAULT 1                           NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP            NOT NULL,
  updated_at TIMESTAMP                                      NULL,
  deleted_at TIMESTAMP                                      NULL
);

CREATE TABLE `products_type` (
  id         INT(11) UNSIGNED                               NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name       VARCHAR(255)                                   NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP            NOT NULL,
  updated_at TIMESTAMP                                      NULL,
  deleted_at TIMESTAMP                                      NULL
);
INSERT INTO products_type (id, name, created_at, updated_at, deleted_at)
VALUES (1, 'Produto', '2016-12-14 10:19:13', NULL, NULL);
INSERT INTO products_type (id, name, created_at, updated_at, deleted_at)
VALUES (2, 'Acessório', '2016-12-14 10:19:13', NULL, NULL);

CREATE TABLE `products` (
  id               INT(11) UNSIGNED                               NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name             VARCHAR(255)                                   NOT NULL,
  products_type_id INT(11) UNSIGNED                               NOT NULL,
  price_buy        DECIMAL(10, 2) DEFAULT 0.00                    NOT NULL,
  price_sell       DECIMAL(10, 2) DEFAULT 0.00                    NOT NULL,
  active           TINYINT(1) DEFAULT 1                           NOT NULL,
  created_at       TIMESTAMP DEFAULT CURRENT_TIMESTAMP            NOT NULL,
  updated_at       TIMESTAMP                                      NULL,
  deleted_at       TIMESTAMP                                      NULL,
  CONSTRAINT fk_products_products_type FOREIGN KEY (products_type_id)
  REFERENCES products_type (id)
);

CREATE TABLE `people` (
  id         INT(11) UNSIGNED                               NOT NULL PRIMARY KEY AUTO_INCREMENT,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP            NOT NULL,
  updated_at TIMESTAMP                                      NULL,
  deleted_at TIMESTAMP                                      NULL
);

CREATE TABLE `building_works` (
  id         INT(11) UNSIGNED                               NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name       VARCHAR(255)                                   NOT NULL,
  people_id  INT(11) UNSIGNED                               NOT NULL,
  active     TINYINT(1) DEFAULT 1                           NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP            NOT NULL,
  updated_at TIMESTAMP                                      NULL,
  deleted_at TIMESTAMP                                      NULL,
  CONSTRAINT fk_building_works_people FOREIGN KEY (people_id)
  REFERENCES people (id)
);

ALTER TABLE people
  ADD COLUMN name VARCHAR(255) NOT NULL
  AFTER id;

ALTER TABLE people
  ADD COLUMN identifyer VARCHAR(100) NOT NULL
  AFTER `name`
,
  ADD COLUMN email VARCHAR(255) NOT NULL
  AFTER `identifyer`
,
  ADD COLUMN phone1 VARCHAR(50) NOT NULL
  AFTER `email`
,
  ADD COLUMN phone2 VARCHAR(50) NOT NULL
  AFTER `phone1`
,
  ADD COLUMN zip_code VARCHAR(100) NOT NULL
  AFTER `phone2`
,
  ADD COLUMN address VARCHAR(255) NOT NULL
  AFTER `zip_code`
,
  ADD COLUMN number VARCHAR(100) NOT NULL
  AFTER `address`
,
  ADD COLUMN address_extra VARCHAR(100) NOT NULL
  AFTER `number`
,
  ADD COLUMN city VARCHAR(255) NOT NULL
  AFTER `address_extra`
,
  ADD COLUMN state VARCHAR(10) NOT NULL
  AFTER `city`
,
  ADD COLUMN active TINYINT(1) DEFAULT 1 NOT NULL
  AFTER state;

ALTER TABLE people
  CHANGE COLUMN number address_number VARCHAR(100) NOT NULL
  AFTER `address`;

ALTER TABLE people
  CHANGE COLUMN identifyer identifier VARCHAR(100) NOT NULL
  AFTER `name`;

ALTER TABLE products
  ADD COLUMN type TINYINT(1) DEFAULT 0 NOT NULL
  AFTER products_type_id;

INSERT INTO products (id, name, products_type_id, type, price_buy, price_sell, active, created_at, updated_at, deleted_at)
VALUES (1, 'Alumínio', 1, 0, 0, 74.19, 1, '2016-12-22 16:58:55', NULL, NULL);
INSERT INTO products (name, products_type_id, type, price_buy, price_sell, active, created_at, updated_at, deleted_at)
VALUES ('Frete', 1, 0, 0, 0.5, 1, '2017-01-03 08:42:54', NULL, NULL);
INSERT INTO products (name, products_type_id, type, price_buy, price_sell, active, created_at, updated_at, deleted_at)
VALUES ('Anodização / Pintura', 1, 0, 0, 2.5, 1, '2017-01-03 08:42:54', NULL, NULL);
INSERT INTO products (name, products_type_id, type, price_buy, price_sell, active, created_at, updated_at, deleted_at)
VALUES ('Custo de instalação', 1, 1, 0, 20, 1, '2017-01-03 08:42:54', NULL, NULL);
INSERT INTO products (name, products_type_id, type, price_buy, price_sell, active, created_at, updated_at, deleted_at)
VALUES ('Custo de instalação 2', 1, 1, 0, 0, 1, '2017-01-03 08:42:54', NULL, NULL);
INSERT INTO products (name, products_type_id, type, price_buy, price_sell, active, created_at, updated_at, deleted_at)
VALUES ('Silicone', 2, 0, 0, 0, 1, '2017-01-03 08:45:38', NULL, NULL);
INSERT INTO products (name, products_type_id, type, price_buy, price_sell, active, created_at, updated_at, deleted_at)
VALUES ('Sikadur', 2, 0, 0, 0, 1, '2017-01-03 08:45:38', NULL, NULL);
INSERT INTO products (name, products_type_id, type, price_buy, price_sell, active, created_at, updated_at, deleted_at)
VALUES ('Chumbador', 2, 0, 0, 0, 1, '2017-01-03 08:45:38', NULL, NULL);
INSERT INTO products (name, products_type_id, type, price_buy, price_sell, active, created_at, updated_at, deleted_at)
VALUES ('Parafuso', 2, 0, 0, 0, 1, '2017-01-03 08:45:38', NULL, NULL);
INSERT INTO products (name, products_type_id, type, price_buy, price_sell, active, created_at, updated_at, deleted_at)
VALUES ('Fita', 2, 0, 0, 0, 1, '2017-01-03 08:45:38', NULL, NULL);
INSERT INTO products (name, products_type_id, type, price_buy, price_sell, active, created_at, updated_at, deleted_at)
VALUES ('Guarnição', 2, 0, 0, 0, 1, '2017-01-03 08:45:38', NULL, NULL);
INSERT INTO products (name, products_type_id, type, price_buy, price_sell, active, created_at, updated_at, deleted_at)
VALUES ('Vulcanização', 2, 0, 0, 0, 1, '2017-01-03 08:45:38', NULL, NULL);
INSERT INTO products (name, products_type_id, type, price_buy, price_sell, active, created_at, updated_at, deleted_at)
VALUES ('Curvatura', 2, 0, 0, 0, 1, '2017-01-03 08:45:38', NULL, NULL);
INSERT INTO products (name, products_type_id, type, price_buy, price_sell, active, created_at, updated_at, deleted_at)
VALUES ('Chapas', 2, 0, 0, 0, 1, '2017-01-03 08:45:38', NULL, NULL);
INSERT INTO products (name, products_type_id, type, price_buy, price_sell, active, created_at, updated_at, deleted_at)
VALUES ('Grelhas', 2, 0, 0, 0, 1, '2017-01-03 08:45:38', NULL, NULL);
INSERT INTO products (name, products_type_id, type, price_buy, price_sell, active, created_at, updated_at, deleted_at)
VALUES ('Routiamento de ACM', 2, 0, 0, 0, 1, '2017-01-03 08:45:38', NULL, NULL);
UPDATE products
SET products_type_id = 1, type = 2
WHERE name = 'Usinagem / Grauteamento';

UPDATE products
SET price_buy = price_sell;
UPDATE products
SET price_buy = 13.00, price_sell = 74.19
WHERE id = 1;

ALTER TABLE building_works
  ADD COLUMN calculator LONGTEXT NULL
  AFTER people_id;

### 20170109

ALTER TABLE products
  DROP COLUMN price_sell,
  CHANGE COLUMN price_buy cost DECIMAL(10, 2) NOT NULL
  AFTER type;

ALTER TABLE `lines`
  DROP COLUMN price_sell,
  CHANGE COLUMN price_buy cost DECIMAL(10, 2) NOT NULL
  AFTER name;

ALTER TABLE `lines`
  ADD COLUMN cost_production DECIMAL(10, 2) NOT NULL DEFAULT 0
  AFTER cost,
  ADD COLUMN cost_installation DECIMAL(10, 2) NOT NULL DEFAULT 0
  AFTER cost_production;

ALTER TABLE `lines`
  DROP COLUMN cost_production,
  CHANGE COLUMN cost cost_production DECIMAL(10, 2) NOT NULL
  AFTER name;

UPDATE products
SET active = 0
WHERE `name` LIKE 'Custo de instala%';

ALTER TABLE products
  MODIFY COLUMN cost DECIMAL(10, 2) NOT NULL DEFAULT 0
  AFTER type;

ALTER TABLE products
  MODIFY COLUMN name VARCHAR(255) NOT NULL DEFAULT ''
  AFTER id;

ALTER TABLE building_works
  MODIFY COLUMN name VARCHAR(255) NOT NULL DEFAULT ''
  AFTER id;

ALTER TABLE `lines`
  MODIFY COLUMN name VARCHAR(255) NOT NULL DEFAULT ''
  AFTER id,
  MODIFY COLUMN cost_production DECIMAL(10, 2) NOT NULL DEFAULT 0
  AFTER name;

ALTER TABLE people
  MODIFY COLUMN name VARCHAR(255) NOT NULL DEFAULT ''
  AFTER id,
  MODIFY COLUMN identifier VARCHAR(100) NOT NULL DEFAULT ''
  AFTER `name`
,
  MODIFY COLUMN email VARCHAR(255) NULL
  AFTER `identifier`
,
  MODIFY COLUMN phone1 VARCHAR(50) NULL
  AFTER `email`
,
  MODIFY COLUMN phone2 VARCHAR(50) NULL
  AFTER `phone1`
,
  MODIFY COLUMN zip_code VARCHAR(100) NULL
  AFTER `phone2`
,
  MODIFY COLUMN address VARCHAR(255) NULL
  AFTER `zip_code`
,
  MODIFY COLUMN address_number VARCHAR(100) NULL
  AFTER `address`
,
  MODIFY COLUMN address_extra VARCHAR(100) NULL
  AFTER `address_number`
,
  MODIFY COLUMN city VARCHAR(255) NULL
  AFTER `address_extra`
,
  MODIFY COLUMN state VARCHAR(10) NULL
  AFTER `city`;

ALTER TABLE products
  ADD COLUMN calculator_type TINYINT(1) NULL
  AFTER type;

UPDATE products
SET calculator_type = 0
WHERE name = 'Alumínio';
UPDATE products
SET calculator_type = 0
WHERE name LIKE 'Anodização %';

INSERT INTO products (name, products_type_id, type, calculator_type, cost, active, created_at, updated_at, deleted_at)
VALUES ('Vidro M2', 1, 1, 1, 253.17, 1, NOW(), NULL, NULL);

DELETE FROM products
WHERE name = 'Usinagem / Grauteamento';
INSERT INTO products (name, products_type_id, type, calculator_type, cost, active, created_at, updated_at, deleted_at)
VALUES ('Usinagem / Grauteamento', 1, 2, 2, 0, 1, NOW(), NULL, NULL);

################### 20170117

CREATE TABLE users
(
  id             INT(10) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
  name           VARCHAR(255)                 NOT NULL,
  email          VARCHAR(255)                 NOT NULL,
  password       VARCHAR(255)                 NOT NULL,
  remember_token VARCHAR(100),
  created_at     TIMESTAMP,
  updated_at     TIMESTAMP
);
CREATE UNIQUE INDEX users_email_unique
  ON users (email);
CREATE TABLE password_resets
(
  email      VARCHAR(255) NOT NULL,
  token      VARCHAR(255) NOT NULL,
  created_at TIMESTAMP
);
CREATE INDEX password_resets_email_index
  ON password_resets (email);
CREATE INDEX password_resets_token_index
  ON password_resets (token);

INSERT INTO users (id, name, email, password, remember_token, created_at, updated_at) VALUES
  (1, 'sistema', 'suporte@emiolo.com', '$2y$10$p13C/YOAp7mBBYDTxooXqemISGFWSwb15Fybs0zhhNiJaJx2EeU2C', NULL, NOW(),
   NOW());
INSERT INTO almais.users (name, email, password, remember_token, created_at, updated_at)
VALUES ('Adauto', 'adauto', '$2y$10$gUfmzILx0iVcVyulE/wGcOKt6Xjn16rfinxL.kUkU/onGnt93k8Ea', NULL, NULL, NULL);
INSERT INTO almais.users (name, email, password, remember_token, created_at, updated_at)
VALUES ('Matheus', 'matheus', '$2y$10$VNl9.AdDUk7/emMs2RoQiuTat3kpuezEF4.m6iCZK5LlAhcwBamaS', NULL, NULL, NULL);
INSERT INTO almais.users (name, email, password, remember_token, created_at, updated_at)
VALUES ('Fabiano', 'fabiano', '$2y$10$4q4REtJj0.wptpDEz4XTdeCjqzWiEeBL98IVAVY5.ala5CQB9trha', NULL, NULL, NULL);
INSERT INTO almais.users (name, email, password, remember_token, created_at, updated_at)
VALUES ('Almais', 'almais', '$2y$10$Z0kGI1qCLaZ9O0rdrcfsSehj9VlCsSzOzpSNacyLk3INaB6IVLnpe', NULL, NULL, NULL);
INSERT INTO almais.users (name, email, password, remember_token, created_at, updated_at)
VALUES ('Almais 2', 'almais2', '$2y$10$/ixRpuiznnORJKq5EAbzje52zDc8xpfy5yP8f1Xs9i3mOxfsHjmsq', NULL, NULL, NULL);

ALTER TABLE `lines`
  ADD COLUMN type TINYINT NOT NULL DEFAULT 0
  AFTER cost_installation;

############################################## 20170125

CREATE TABLE `company_fixed_costs` (
  id         INT(11) UNSIGNED                               NOT NULL PRIMARY KEY AUTO_INCREMENT,
  month      TINYINT(1)                                     NOT NULL,
  year       TINYINT(1)                                     NOT NULL,
  cost       DECIMAL(10, 2) DEFAULT 0.00                    NOT NULL,
  current    TINYINT(1) DEFAULT 1                           NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP            NOT NULL,
  updated_at TIMESTAMP                                      NULL,
  deleted_at TIMESTAMP                                      NULL
);

ALTER TABLE company_fixed_costs
  MODIFY COLUMN year INT(11) NOT NULL
  AFTER month;