<?php

namespace App\Domains\Main\Http\Controllers;

use App\Domains\Main\Models\Person;
use App\Http\Controllers\CerebeloController;
use App\User;
use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;

class UserController extends CerebeloController
{
    /**
     * @var User
     */
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;

        parent::__construct();
    }

    public function index($id = null)
    {
        $model = $this->user->find($id);

        return view('cerebelo.users', ['model' => $model]);
    }

    public function store(Request $request)
    {
        $id = $request->get('id', null);

        $update = true;
        $user = $this->user->find($id);
        if (!$user) {
            $user = new Person();
            $update = false;
        }

        $user->fill($this->clearEntry($request->all()));
        $saved = $user->save();

        if ($saved) {
            if ($update) {
                return $this->redirectAfterSuccess("Registro atualizado com sucesso", "users.index", $user->id);
            }
            return $this->redirectAfterSuccess("Registro inserido com sucesso", "users.index", $user->id);
        }

        return $this->redirectBackAfterError(trans('messages.error.crud'));
    }

    public function datatables()
    {
        $result = $this->user->all();

        return Datatables::of($result)
            ->addColumn('actions', function ($object) {
                return '';
            })
            ->addColumn('deleted_at', function ($object) {
                return '';
            })
            ->make(true);
    }
}