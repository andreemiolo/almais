<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'clients'], function () {
    Route::get('/{id?}', ['as' => 'cerebelo.clients.index', 'uses' => 'ClientController@index'])->where(['id' => '[0-9]*']);
    Route::get('/view/{id}', ['as' => 'cerebelo.clients.view', 'uses' => 'ClientController@viewData'])->where(['id' => '[0-9]*']);
    Route::post('/{id?}', ['as' => 'cerebelo.clients.store', 'uses' => 'ClientController@store'])->where(['id' => '[0-9]*']);
    Route::get('/datatables', ['as' => 'cerebelo.clients.datatables', 'uses' => 'ClientController@datatables']);
});


Route::group(['prefix' => 'users'], function () {
    Route::get('/{id?}', ['as' => 'cerebelo.users.index', 'uses' => 'UserController@index'])->where(['id' => '[0-9]*']);
    Route::post('/{id?}', ['as' => 'cerebelo.users.store', 'uses' => 'UserController@store'])->where(['id' => '[0-9]*']);
    Route::get('/datatables', ['as' => 'cerebelo.users.datatables', 'uses' => 'UserController@datatables']);
});