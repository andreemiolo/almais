<?php

namespace App\Domains\Sales\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Product extends Eloquent
{
    const TYPE_KILO = 0;
    const TYPE_METERS = 1;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'products_type_id',
        'type',
        'cost',
        'active',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'price',
        'price_formatted',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * Relacionamento com o tipo do produto
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function productType()
    {
        return $this->belongsTo(ProductType::class, 'products_type_id');
    }

    /**
     * Retorna somente os ativos
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where("active", 1);
    }

    public function getPriceAttribute()
    {
        return (float)$this->cost;
    }

    public function getPriceFormattedAttribute()
    {
        return number_format($this->price, 2, ',', '.');
    }

    /**
     * Busca os dados para um input
     *
     * @param int $products_type_id
     * @return array
     */
    public static function inputSelect($products_type_id)
    {
        return self::active()
            ->where('products_type_id', $products_type_id)
            ->orderBy('name', 'asc')
            ->pluck('name', 'id')->all();
    }

    protected function fillProducts(Product $product)
    {
        $getData = [
            'id',
            'name',
            'price',
            'price_formatted',
            'type'
        ];

        $fill = [
            'calculated' => false
        ];

        $response = [];
        foreach ($getData as $index => $data) {
            if ($data == 'type' && $product->$data > 1) {
                $product->$data = 1;
            }

            $value = $product->$data;
            $position = is_numeric($index) ? $data : $index;

            $response[$position] = $value;
        }

        return ($response + $fill);
    }

    protected function fillAccessories(Product $product)
    {
        $getData = [
            'id',
            'name',
            'price',
            'price_formatted',
        ];

        $fill = [
            'quantity' => 0,
            'taxe' => true,
            'print' => true,
            'calculated' => false
        ];

        $response = [];
        foreach ($getData as $index => $data) {
            $value = $product->$data;
            $position = is_numeric($index) ? $data : $index;

            $response[$position] = $value;
        }

        return ($response + $fill);
    }

    /**
     * Retorna um array baseando-se no tipo do produto
     *
     * @param $product_type_id
     * @param $calculator_type
     * @param null $id
     * @return array
     */
    public function fromType($product_type_id, $calculator_type, $id = null)
    {
        $handle = self::select(['id', 'name', 'cost', 'type'])
            ->where('products_type_id', $product_type_id)
            ->where(function ($query) use ($calculator_type) {
                $query->where('calculator_type', $calculator_type)
                    ->orWhereNull('calculator_type');
            })
            ->whereRaw($id > 0 ? "id = " . (int)$id : "active")
            ->orderByRaw("calculator_type IS NOT NULL DESC, id ASC");

        if ($product_type_id == 1) {
            // Se a calculadora não é do tipo ACM
//            if ($calculator_type != 2) {
//                $handle = $handle->where('type', '<>', 2);
//            }
        } else if ($product_type_id == 2) {
            $handle = $handle->inRandomOrder()->limit(5);
        }

        $products = $handle->get();
        $response = [];

        $method = 'fillProducts';
        if ($product_type_id == 2) {
            $method = 'fillAccessories';
        }

        foreach ($products as $product) {
            $response[] = $this->$method($product);
        }

        return $response;
    }

    public function save(array $options = [])
    {
        if (!is_null($this->calculator_type)) {
            $this->active = 1;
        }

        return parent::save($options);
    }
}