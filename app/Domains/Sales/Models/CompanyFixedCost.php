<?php
/**
 * Created by PhpStorm.
 * User: emiolo
 * Date: 25/01/17
 * Time: 14:53
 */

namespace App\Domains\Sales\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class CompanyFixedCost extends Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'month',
        'year',
        'cost',
        'current',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'company_fixed_costs';

    /**
     * Retorna o último
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCurrent($query)
    {
        return $query->where("current", 1);
    }

    public function save(array $options = [])
    {
        function number_unformat($number, $force_number = true, $dec_point = '.', $thousands_sep = ',') {
            if ($force_number) {
                $number = preg_replace('/^[^\d]+/', '', $number);
            } else if (preg_match('/^[^\d]+/', $number)) {
                return false;
            }
            $type = (strpos($number, $dec_point) === false) ? 'int' : 'float';
            $number = str_replace(array($dec_point, $thousands_sep), array('.', ''), $number);
            settype($number, $type);
            return $number;
        }

        CompanyFixedCost::current()->update([
            'current' => 0
        ]);

        $this->cost = (float) (str_replace([',', '.'], '', $this->cost) / 100.0);
        $this->current = 1;

        return parent::save($options);
    }
}