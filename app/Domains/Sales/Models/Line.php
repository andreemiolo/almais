<?php

namespace App\Domains\Sales\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Line extends Eloquent
{
    const TYPE_ALUMINIUM = 0;
    const TYPE_GLASS = 1;
    const TYPE_ACM = 2;

    private $calculator_types = [
        self::TYPE_ALUMINIUM => 'Alumínio',
        self::TYPE_GLASS => 'Vidro',
        self::TYPE_ACM => 'ACM',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'cost_production',
        'cost_installation',
        'type',
        'active',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'description'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'lines';

    public function getDescriptionAttribute()
    {
        return $this->calculator_types[$this->type] . ' ' . $this->name . ' - ' .
            'R$ ' . number_format($this->cost_production, 2, ',', '.');
    }

    /**
     * Retorna somente os ativos
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where("active", 1);
    }

    /**
     * Busca os dados para um input
     *
     * @return array
     */
    public static function inputSelect()
    {
        return self::active()
            ->orderBy('name', 'asc')
            ->get()
            ->pluck('description', 'id')
            ->all();
    }

    /**
     * @return array
     */
    public function getCalculatorTypes()
    {
        return $this->calculator_types;
    }
}