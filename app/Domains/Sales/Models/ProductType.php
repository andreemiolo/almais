<?php

namespace App\Domains\Sales\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class ProductType extends Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products_type';

    /**
     * Busca os tipos de produto ativos no sistema
     *
     * @return array
     */
    public static function inputSelect()
    {
        $return = self::orderBy('name', 'asc')
            ->pluck('name', 'id');

        return array_set($return, '', 'Tipo de produto');
    }
}