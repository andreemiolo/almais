<?php

namespace App\Domains\Sales\Http\Controllers;

use App\Domains\Sales\Models\Product;
use App\Domains\Sales\Models\ProductType;
use App\Http\Controllers\CerebeloController;
use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;

class ProductController extends CerebeloController
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var ProductType
     */
    protected $productType;

    public function __construct(Product $product, ProductType $productType)
    {
        $this->product = $product;
        $this->productType = $productType;

        parent::__construct();
    }

    public function index($id = null)
    {
        $product = $this->product->find($id);
        $productTypes = $this->productType->inputSelect();

        return view('cerebelo.products', ['model' => $product, 'productTypes' => $productTypes]);
    }

    public function store(Request $request)
    {
        $id = $request->get('id', null);

        $update = true;
        $product = $this->product->find($id);
        if (!$product) {
            $product = new Product();
            $update = false;
        }

        $variables = $request->all();
        foreach ($variables as $variable => $value) {
            if (empty($value)) {
                unset($variables[$variable]);
            }
        }
        if (isset($variables['cost'])) {
            $source = array('.', ',');
            $replace = array('', '.');
            $variables['cost'] = str_replace($source, $replace, $variables['cost']);
        }
        $product->fill($variables);

        $saved = $product->save();

        if ($saved) {
            if ($update) {
                return $this->redirectAfterSuccess("Registro atualizado com sucesso", "products.index");
            }
            return $this->redirectAfterSuccess("Registro inserido com sucesso", "products.index");
        }

        return $this->redirectBackAfterError("Houve algum problema ao inserir o registro");
    }

    public function active(Request $request, $id)
    {
        $product = $this->product->find($id);

        $product->fill($request->all());
        $product->save();

        return $this->redirectAfterSuccess("Registro atualizado com sucesso", "products.index");
    }

    public function datatables()
    {
        $result = $this->product->select('*');

        return Datatables::of($result)
            ->addColumn('active', function ($object) {
                $active = '<span class="label label-success">Ativa</span>';
                if (!$object->active) {
                    $active = '<span class="label label-danger">Inativa</span>';
                }
                return $active;
            })
            ->addColumn('products_type_id', function ($object) {
                return $object->productType->name;
            })
            ->addColumn('actions', function ($object) {
                return '<ul class="icons-list">
                            <li class="dropdown">
                                <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a class="editar text-info-600" href="' . route('cerebelo.products.index', $object->id) . '">
                                            <i class="icon-pencil3"></i>
                                            Editar
                                        </a>
                                    </li>'
                    . (is_null($object->calculator_type) ?
                        '<li>
                                        <a
                                        class="' . ($object->active ? 'ina' : 'a') . 'tivar text-' . ($object->active ? 'slate' : 'green') . '-800"
                                        rel="nofollow" 
                                        data-method="patch" data-confirm="" data-msg="" 
                                        href="' . route('cerebelo.products.active', ['id' => $object->id, 'active' => (int)!$object->active]) . '"
                                        >
                                            <i class="icon-' . ($object->active ? 'blocked' : 'checkmark') . '"></i>
                                            ' . ($object->active ? 'Ina' : 'A') . 'tivar
                                        </a>
                                    </li>
                                </ul>
                            </li>' : '')
                    .
                    '</ul>';
            })
            ->make(true);
    }
}