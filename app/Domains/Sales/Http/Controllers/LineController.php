<?php

namespace App\Domains\Sales\Http\Controllers;

use App\Domains\Sales\Models\Line;
use App\Http\Controllers\CerebeloController;
use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;

class LineController extends CerebeloController
{
    /**
     * @var Line
     */
    protected $line;

    public function __construct(Line $line)
    {
        $this->line = $line;

        parent::__construct();
    }

    public function index($id = null)
    {
        $line = $this->line->find($id);

        return view('cerebelo.line', ['model' => $line, 'calculator_types' => $this->line->getCalculatorTypes()]);
    }

    public function store(Request $request)
    {
        $id = $request->get('id', null);

        $update = true;
        $line = $this->line->find($id);
        if (!$line) {
            $line = new Line();
            $update = false;
        }

        $line->fill($this->clearEntry($request->all()));
        $saved = $line->save();

        if ($saved) {
            if ($update) {
                return $this->redirectAfterSuccess("Registro atualizado com sucesso", "line.index");
            }
            return $this->redirectAfterSuccess("Registro inserido com sucesso", "line.index");
        }

        return $this->redirectBackAfterError(trans('messages.error.crud'));
    }

    public function active(Request $request, $id)
    {
        $line = $this->line->find($id);

        $line->fill($request->all());
        $line->save();

        return $this->redirectAfterSuccess("Registro atualizado com sucesso", "line.index");
    }

    public function datatables()
    {
        $result = $this->line->select('*');

        return Datatables::of($result)
            ->addColumn('active', function ($object) {
                $active = '<span class="label label-success">Ativa</span>';
                if (!$object->active) {
                    $active = '<span class="label label-danger">Inativa</span>';
                }
                return $active;
            })
            ->addColumn('actions', function ($object) {
                return '<ul class="icons-list">
                            <li class="dropdown">
                                <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a class="editar text-info-600" href="' . route('cerebelo.line.index') . '/' . $object->id . '">
                                            <i class="icon-pencil3"></i>
                                            Editar
                                        </a>
                                    </li>
                                    <li>
                                        <a
                                        class="' . ($object->active ? 'ina' : 'a') . 'tivar text-' . ($object->active ? 'slate' : 'green') . '-800"
                                        rel="nofollow" 
                                        data-method="patch" data-confirm="" data-msg="" 
                                        href="' . route('cerebelo.line.active', ['id' => $object->id, 'active' => (int)!$object->active]) . '"
                                        >
                                            <i class="icon-' . ($object->active ? 'blocked' : 'checkmark') . '"></i>
                                            ' . ($object->active ? 'Ina' : 'A') . 'tivar
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>';
            })
            ->make(true);
    }
}