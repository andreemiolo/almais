<?php

namespace App\Domains\Sales\Http\Controllers;

use App\Domains\Sales\Models\CompanyFixedCost;
use App\Http\Controllers\CerebeloController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class CompanyFixedCostController extends CerebeloController
{
    /**
     * @var CompanyFixedCost
     */
    protected $companyFixedCost;

    public function __construct(CompanyFixedCost $companyFixedCost)
    {
        $this->companyFixedCost = $companyFixedCost;

        parent::__construct();
    }

    public function index()
    {
        $companyFixedCost = $this->companyFixedCost->current()->first();

        $months = [];
        for ($i = 1; $i <= 12; $i++) {
            $months[$i] = $i;
        }

        $years = [];
        for ($i = 2016; $i <= date('Y'); $i++) {
            $years[$i] = $i;
        }

        return view('cerebelo.fixed_costs', [
            'model' => $companyFixedCost,
            'months' => $months,
            'years' => $years,
        ]);
    }

    public function store(Request $request)
    {
        $id = $request->get('id', null);

        $update = true;
        $companyFixedCost = $this->companyFixedCost->find($id);
        if (!$companyFixedCost) {
            $companyFixedCost = new CompanyFixedCost();
            $update = false;
        }

        $variables = $request->all();
        foreach ($variables as $variable => $value) {
            if (empty($value)) {
                unset($variables[$variable]);
            }
        }

        $companyFixedCost->fill($variables);

        $saved = $companyFixedCost->save();

        if ($saved) {
            if ($update) {
                return $this->redirectAfterSuccess("Registro atualizado com sucesso", "fixed-costs.index");
            }
            return $this->redirectAfterSuccess("Registro inserido com sucesso", "fixed-costs.index");
        }

        return $this->redirectBackAfterError("Houve algum problema ao inserir o registro");
    }

    public function datatables()
    {
        $result = $this->companyFixedCost
            ->select('*')
            ->orderBy('current', 'DESC')
            ->orderBy(DB::raw('CONCAT(year, month)'), 'DESC');

        return Datatables::of($result)
            ->addColumn('current', function ($object) {
                $active = '<span class="label label-success">Atual</span>';
                if (!$object->current) {
                    $active = '<span class="label label-danger">Histórico</span>';
                }
                return $active;
            })
            ->addColumn('cost', function ($object) {
                return number_format($object->cost, 2, ',', '.');
            })
            ->make(true);
    }
}