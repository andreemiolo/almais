<?php

namespace App\Domains\Sales\Http\Controllers;

use App\Domains\Main\Models\Person;
use App\Domains\Sales\Models\BuildingWork;
use App\Http\Controllers\CerebeloController;
use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;

class BuildingWorkController extends CerebeloController
{
    /**
     * @var BuildingWork
     */
    protected $buildingWork;

    /**
     * @var Person
     */
    protected $person;

    public function __construct(BuildingWork $buildingWork, Person $person)
    {
        $this->buildingWork = $buildingWork;
        $this->person = $person;

        parent::__construct();
    }

    public function index($id = null)
    {
        $buildingWork = $this->buildingWork->find($id);
        $people = $this->person->inputSelect();

        return view('cerebelo.building_works', ['model' => $buildingWork, 'people' => $people]);
    }

    public function client($id)
    {
        $people = $this->person->inputSelect($id);

        return view('cerebelo.building_works', ['model' => $this->buildingWork, 'people' => $people]);
    }

    public function budgets()
    {
        return view('cerebelo.budgets');
    }

    public function store(Request $request)
    {
        $id = $request->get('id', null);

        $update = true;
        $buildingWork = $this->buildingWork->find($id);
        if (!$buildingWork) {
            $buildingWork = new BuildingWork();
            $update = false;
        }

        $variables = $request->all();
        foreach ($variables as $variable => $value) {
            if (empty($value)) {
                unset($variables[$variable]);
            }
        }
        $buildingWork->fill($variables);
        $saved = $buildingWork->save();

        if ($saved) {

            if (isset($variables['calc']) && $variables['calc']) {
                return $this->redirectAfterSuccess("Registro atualizado com sucesso", "calculator.budget", $buildingWork->id);
            }

            if ($update) {
                return $this->redirectAfterSuccess("Registro atualizado com sucesso", "building-works.index");
            }

            return $this->redirectAfterSuccess("Registro inserido com sucesso", "building-works.index");
        }

        return $this->redirectBackAfterError(trans('messages.error.crud'));
    }

    public function active(Request $request, $id)
    {
        $buildingWork = $this->buildingWork->find($id);

        $buildingWork->fill($request->all());
        $buildingWork->save();

        return $this->redirectAfterSuccess("Registro atualizado com sucesso", "building-works.index");
    }

    public function datatables($type = null)
    {
        $result = $this->buildingWork->all();

        return Datatables::of($result)
            ->addColumn('active', function ($object) {
                $active = '<span class="label label-success">Ativa</span>';
                if (!$object->active) {
                    $active = '<span class="label label-danger">Inativa</span>';
                }
                return $active;
            })
            ->addColumn('people_id', function ($object) {
                return $object->client->name;
            })
            ->addColumn('actions', function ($object) use ($type) {
                if ($type == 'budget') {
                    return '<a class="text-info-600" href="' . route('cerebelo.calculator.budget', $object->id) . '" title="Calculadora">
                                <i class="icon-calculator"></i>
                            </a>';
                }

                return '<ul class="icons-list">
                            <li class="dropdown">
                                <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a class="editar text-info-600" href="' . route('cerebelo.building-works.index', $object->id) . '">
                                            <i class="icon-pencil3"></i>
                                            Editar
                                        </a>
                                    </li>
                                    <li>
                                        <a
                                        class="' . ($object->active ? 'ina' : 'a') . 'tivar text-' . ($object->active ? 'slate' : 'green') . '-800"
                                        rel="nofollow" 
                                        data-method="patch" data-confirm="" data-msg="" 
                                        href="' . route('cerebelo.building-works.active', ['id' => $object->id, 'active' => (int)!$object->active]) . '"
                                        >
                                            <i class="icon-' . ($object->active ? 'blocked' : 'checkmark') . '"></i>
                                            ' . ($object->active ? 'Ina' : 'A') . 'tivar
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>';
            })
            ->make(true);
    }
}