<?php

namespace App\Domains\Sales\Http\Controllers;

use App\Domains\Main\Models\Person;
use App\Domains\Sales\Models\BuildingWork;
use App\Domains\Sales\Models\CompanyFixedCost;
use App\Domains\Sales\Models\Line;
use App\Domains\Sales\Models\Product;
use App\Http\Controllers\CerebeloController;
use Illuminate\Http\Request;

class CalculatorController extends CerebeloController
{
    /**
     * @var BuildingWork
     */
    protected $buildingWork;

    /**
     * @var Person
     */
    protected $person;

    /**
     * @var Line
     */
    protected $line;

    /**
     * @var Product
     */
    protected $product;

    /**
     * @var CompanyFixedCost
     */
    protected $companyFixedCost;

    /**
     * CalculatorController constructor.
     * @param BuildingWork $buildingWork
     * @param Person $client
     * @param Line $line
     * @param Product $product
     * @param CompanyFixedCost $companyFixedCost
     */
    public function __construct(
        BuildingWork $buildingWork,
        Person $client,
        Line $line,
        Product $product,
        CompanyFixedCost $companyFixedCost
    )
    {
        $this->buildingWork = $buildingWork;
        $this->person = $client;
        $this->line = $line;
        $this->product = $product;

        $this->companyFixedCost = $companyFixedCost;
        if (!$companyFixedCost->cost) {
            $this->companyFixedCost = $companyFixedCost->current()->first();
        }

        parent::__construct();
    }

    public function calculator($id = null)
    {
        $new = true;
        $buildingWork = $this->buildingWork->find($id);
        $client = new Person();
        if ($buildingWork) {
            $client = $buildingWork->client;
            $new = false;
        }

        $aluminium = $this->product->find(1);

        $buildingWorks = $this->buildingWork->inputSelect();
        $people = $this->person->inputSelect();

        $lines = $this->line->inputSelect();

        $accessories = $this->product->inputSelect(2);

        $calculators = $buildingWork->calculator;

        $calculators_array = json_decode($calculators, true);
        $calculators_accessories = [];
        if (isset($calculators_array['calculators'])) {
            foreach ($calculators_array['calculators'] as $id => $data) {
                $c_accessories = $data['accessories'];

                foreach ($c_accessories as $accessory_id => $a_data) {
                    if (!is_numeric($accessory_id)) {
                        continue;
                    }

                    $accessory = $this->product->fromType(2, $data['lineDetails']['calculatorType'], $accessory_id);
                    $calculators_accessories[$id][] = $accessory[0];
                }
            }
        }

        return view('cerebelo.calculator', [
            'new' => $new,
            'calculators' => $calculators ?: '{}',
            'calculators_accessories' => json_encode($calculators_accessories),
            'aluminium_price' => (float)$aluminium->cost,
            'buildingWork' => $buildingWork,
            'client' => $client,
            'buildingWorks' => $buildingWorks,
            'people' => $people,
            'lines' => $lines,
            'accessories' => $accessories,
        ]);
    }

    public function save(Request $request, $id)
    {
        /**
         * @var $buildingWork BuildingWork
         */
        $buildingWork = $this->buildingWork->find($id);

        $all = $request->all();
        unset($all['_token']);

        $buildingWork->calculator = json_encode($all);

        $buildingWork->save();

        return $all;
    }

    public function newCalculator(Request $request)
    {
        $id = $request->get('id');
        $line_id = $request->get('line_id');
        $meters = $request->get('meters');
        $kilo = $request->get('kilo');

        $line = $this->line->find($line_id);
        if (!$line) {
            return $this->json(['error' => true]);
        }
        $type = $line->type;

        $file = file_get_contents(view('cerebelo.calculatorSkeleton')->getPath());

        if (!$id) {
            $id = 'calculator-' . $line->id . '-' . uniqid();
        }

        $contents = str_replace('calculatorID', $id, $file);

        $response = [
            'error' => false,
            'id' => $id,
            'calculatorType' => $type,
            'meters' => $meters,
            'kilo' => $kilo,
            'name' => $line->name,
            'price' => $line->cost_production,
            'installation' => $line->cost_installation,
            'contents' => $contents,
            'company_fixed_cost' => (float)$this->companyFixedCost->cost,
        ];

        return $this->json($response);
    }

    public function data($product_type_id, $calculator_type, $id = null)
    {
        $ids = explode(',', $id);

        $products = [];
        foreach ($ids as $id) {
            $product = $this->product->fromType($product_type_id, $calculator_type, $id);

            while ($product) {
                $add = array_shift($product);
                if ($add) {
                    array_push($products, $add);
                }
            }
        }

        return $this->json($products);
    }
}